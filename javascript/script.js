document.addEventListener('DOMContentLoaded', function() {
    // Add event listener to the button
    document.getElementById('nextScreenBtn').addEventListener('click', function() {
        // Redirect to summary.html in the same tab
        window.location.href = 'process.html';
    });
});

document.addEventListener('DOMContentLoaded', function() {
    fetch('data.json')
        .then(response => response.json())
        .then(data => {
            const tableBody = document.getElementById('tableBody');

            data.phases.forEach(phase => {
                const row = document.createElement('tr');
                const phaseCell = document.createElement('td');
                phaseCell.textContent = phase.name;
                row.appendChild(phaseCell);

                phase.criteria.forEach(criteria => {
                    const criteriaCell = document.createElement('td');
                    criteriaCell.textContent = criteria;
                    row.appendChild(criteriaCell);
                });

                tableBody.appendChild(row);
            });
        })
        .catch(error => console.error('Error fetching data:', error));
});